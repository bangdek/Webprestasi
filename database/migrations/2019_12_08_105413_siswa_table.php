<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->Increments('id_siswa');
            $table->string('nama', 100);
            $table->char('nis', 15)->unique();
            $table->string('email', 100)->nullable();
            $table->text('alamat')->nullabe();
            $table->string('profil', 100)->nullable();
            // $table->char('kode_jurusan', 15)->unique();
            // $table->integer('id_admin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
