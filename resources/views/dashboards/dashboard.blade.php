<!doctype html>
<html lang="en">

@include('partials.header');
@include('partials.navbar');
<body>
    <!-- KONTENT -->
    <div class="container-fluid">
        <div class="row">
            @include('partials.sidebar')
            <!-- Beranda -->
            <div class="col-sm-9 admin-beranda">
                <h1>&nbsp;Selamat Datang</h1><br>
                <p class="text-center">{{Auth::user()->username}}</p>
                @if(auth()->user()->role=='siswa')
                    <img src="{{auth()->user()->siswa->getProfile()}}" alt="foto-profil"><br><br>
                @endif
                @if(auth()->user()->role=='admin')
                    <img src="{{URL::asset('images/profile.png')}}" alt="profile">
                @endif
                <p class="text-center">di SISTEM DATABASE PRESTASI SISWA, semoga dapat membantu pada setiap kegiatan
                    <br>
                    administrasi prestasi siswa SMKN NEGERI 1 BEKASI</p>
            </div>
        </div>
    </div>
@include('partials.footer')

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>