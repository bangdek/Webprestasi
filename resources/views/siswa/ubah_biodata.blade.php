<!doctype html>
<html lang="en">

@include('partials.header');
@include('partials.navbar');

<body>
    <!-- KONTENT -->
    <div class="container-fluid">
        <div class="row">
            @include('partials.sidebar');
            
            <!-- Edit Pengguna -->
            <div class="col-sm-9 admin-ubah-biodata">
                @if(session('sukses'))
                    <div class="alert alert-success" role="alert">
                        {{session('sukses')}}
                    </div>
                @endif 
                <h1>&nbsp;Ubah Biodata</h1><br>
                <div class="form-ubah-biodata">
                    <form action="" method="POST" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group">
                            <p>Nama Siswa</p>
                            <input type="text" name="nama" value="{{$siswa->nama}}" disabled>
                            <p>NIS</p>
                            <input type="text" name="nis" value="{{$siswa->nis}}" disabled><br>
                            <p>Email</p>
                            <input type="text" name="email" value="{{$siswa->email}}"><br>
                            <p>Alamat</p>
                            <input type="text" name="alamat" value="{{$siswa->alamat}}"><br>
                            <p>Jurusan</p>
                            @if($siswa->jurusan_id==1)
                            <input type="text" name="jurusan_id" value="Teknik Komputer dan Jaringan" disabled><br>
                            @endif
                            @if($siswa->jurusan_id==2)
                            <input type="text" name="jurusan_id" value="Rekayasa Perangkat Lunak" disabled><br>
                            @endif
                            @if($siswa->jurusan_id==3)
                            <input type="text" name="jurusan_id" value="Multimedia" disabled><br>
                            @endif
                        </div>
                        <div class="form-group">
                            <button class="tombol-hitam" type="submit">Ubah</button>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>

@include('partials.footer');


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>