<!doctype html>
<html lang="en">

@include('partials.header');
@include('partials.navbar');

<body>


    <!-- KONTENT -->
    <div class="container-fluid">
        <div class="row">
            @include('partials.sidebar');

            <!-- Tambah Pengguna -->
            <div class="col-sm-9 admin-tambah-pengguna">
                <h1>&nbsp;Tambah Pengguna</h1><br>
                <div class="form-tambah-pengguna">
                    <form action="" method="POST" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group">
                            <p>Nama Pengguna</p>
                            <input type="text" name="username">
                            <p>Nama Siswa</p>
                            <input type="text" name="nama">
                            <p>NIS</p>
                            <input type="text" name="nis"><br>
                            <p>Jurusan</p>
                            <!-- <input type="text" name="jurusan"><br> -->
                            <select name="jurusan_id" class="form-group">
                                <option>Pilih Jurusan</option>
                                <option value="1">Teknik Komputer dan Jaringan</option>
                                <option value="2">Rekayasa Perangkat Lunak</option>
                                <option value="3">Multimedia</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button class="tombol-hitam" type="submit">TAMBAH</button>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>

@include('partials.footer');


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>