<!doctype html>
<html lang="en">
@include('partials.header');
@include('partials.navbar');


<body>
    <!-- KONTENT -->
    <div class="container-fluid">
        <div class="row">
            @include('partials.sidebar');
            <!-- Daftar Siswa -->
            <div class="col-sm-9 admin-daftar-pengguna">
                <h1>&nbsp;Daftar Siswa</h1><br>
                <!-- Search form -->
                <form class="form-inline  my-2 my-lg-0" method="GET" action="">
                    <input name="cari" class="form-control mr-sm-2" type="search" placeholder="Cari" aria-label="Cari">
                    <button class="btn btn-outline-warning my-2 my-sm-0" type="submit">Cari</button>
                </form>
                <div class="form-daftar-pengguna">
                    @if(session('sukses'))
                    <div class="alert alert-warning" role="alert">
                        {{session('sukses')}}
                    </div>
                    @endif
                    <br>
                    <form action="">
                        <table class="table table-hover">
                            <thead class="thead-danger">
                                <tr>
                                    <th>NO</th>
                                    <th>NAMA</th>
                                    <th>NIS</th>
                                    <th>EMAIL</th>
                                    <th>ALAMAT</th>
                                    <th>JURUSAN</th>
                                    <th>AKSI</th>
                                </tr>
                            </thead>
                            @foreach($daftar_siswa as $siswa)
                            <tr>
                                <td></td>
                                <td>{{$siswa->nama}}</td>
                                <td>{{$siswa->nis}}</td>
                                <td>{{$siswa->email}}</td>
                                <td>{{$siswa->alamat}}</td>
                                @if($siswa->jurusan_id==1)
                                    <td>Teknik Komputer dan Jaringan</td>   
                                @endif
                                @if($siswa->jurusan_id==2)
                                    <td>Rekayasa Perangkat Lunak</td>
                                @endif
                                @if($siswa->jurusan_id==3)
                                    <td>Multimedia</td>
                                @endif
                                <td><a href ="/siswa/{{$siswa->id}}/ubah" class="btn tombol-edit">Ubah</a>
                                <a href="/siswa/{{$siswa->id}}/hapus" class="btn tombol-hapus" onclick="return confirm('Apakah Anda ingin menghapus data siswa ini?')">Hapus</a></td>
                            </tr>
                            @endforeach
                        </table>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>

@include('partials.footer');


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>