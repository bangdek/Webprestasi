<!-- Sidebar -->
<div class="col-sm-3 sidebar">
    <p>Selamat Datang,<br><span>{{Auth::user()->username}}</span></p>
    @if(auth()->user()->role=='siswa')
        <img src="{{auth()->user()->siswa->getProfile()}}" alt="profile"><br>
    @endif
    @if(auth()->user()->role=='admin')
        <img src="{{URL::asset('images/profile.png')}}" alt="profile"><br>
    @endif
    <i class="fa fa-home"></i><a href="/dashboard">Beranda</a><br>
    <i class="fa fa-key"></i><a href="/ubah_password">Ubah Kata Sandi</a><br>
    @if(auth()->user()->role=='siswa')
    <i class="fa fa-photo"></i><a href="/ubah_foto">Ubah Foto Profil</a><br>
    <i class="fa fa-user"></i><a href="/ubah_biodata">Ubah Biodata</a><br>
    <i class="fa fa-plus-circle"></i><a href="/tambah_prestasi">Tambah Prestasi</a><br>
    <i class="fa fa-list"></i><a href="/prestasi">Riwayat Prestasi</a><br>
    @endif
    @if(auth()->user()->role=='admin')
    <i class="fa fa-photo"></i><a href="admin/ubah_foto">Ubah Foto Profil</a><br>
    <i class="fa fa-user-plus"></i><a href="/tambah_siswa">Tambah Pengguna</a><br> 
    <i class="fa fa-plus-circle"></i><a href="/daftar_siswa">Daftar Pengguna</a><br>
    <i class="fa fa-list"></i><a href="/daftar_prestasi">Daftar Prestasi</a><br>
    @endif
    <a href="/logout" class="btn btn-danger">Keluar</a>
</div>