<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="{{URL::asset('img/logo.png')}}" alt="logo">SMK NEGERI 1 BEKASI</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <i class="navbar-brand ml-auto" href="#">DATABASE PRESTASI SISWA</i>
    </div>
</nav>