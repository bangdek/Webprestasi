<!doctype html>
<html lang="en">

@include('partials.header');
@include('partials.navbar');

<body>
    <!-- KONTENT -->
    <div class="container-fluid">
        <div class="row">
            @include('partials.sidebar');

            <!-- Daftar Pengguna -->
            
            <div class="col-sm-9 pengguna-tambah-prestasi">
            @if(session('sukses'))
                    <div class="alert alert-success" role="alert">
                        {{session('sukses')}}
                    </div>
                @endif 
                <h1>&nbsp;Tambah Prestasi</h1>
                <div class="form-tambah-prestasi">
                    <form action="" method="POST" role="form">
                        <div class="row">
                            <div class="col-sm-6">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <p>Nama Kegiatan</p>
                                <input type="text" name="nama_kegiatan">
                                <p>Peringkat</p>
                                <input type="number" name="peringkat">
                                <p>Tingkat</p>
                                <input type="text" name="tingkat">
                                <p>Penyelenggara</p>
                                <input type="text" name="penyelenggara">
                                <p>Jumlah Peserta</p>
                                <input type="number" name="jml_peserta">
                                <p>Tanggal Mulai Pelaksanaan</p>
                                <input type="date" name="tgl_mulai">
                                <p>Tanggal Selesai Pelaksanaan</p>
                                <input type="date" name="tgl_akhir">
                                <p>URL / Website Perlombaan</p>
                                <input type="text" name="url_panitia">
                            </div>
                            <div class="col-sm-6 unggah-sertifikat">
                                <p>Unggah Sertifikat/Piala/Medali</p><br>
                                <div class="custom-file">
                                    <input name="foto_reward" type="file" class="custom-file-input" id="customFile" accept=".jpg, .png, .jpeg, .pdf">
                                    <label class="custom-file-label" for="customFile">Pilih sertifikat/piala/medali</label>
                                </div>
                                <p>Unggah foto pemberian hadiah</p><br>
                                <div class="custom-file">
                                    <input name="foto_penyerahan" type="file" class="custom-file-input" id="customFile" accept=".jpg, .png, .jpeg, .pdf">
                                    <label class="custom-file-label" for="customFile">Pilih foto pemberian hadiah</label>
                                </div>
                                <button class="btn tombol-hitam" type="submit">SIMPAN</button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

@include('partials.footer');


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>

<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>