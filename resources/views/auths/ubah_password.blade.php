<!doctype html>
<html lang="en">

@include('partials.header');
@include('partials.navbar');
<body>

    <!-- KONTENT -->
    <div class="container-fluid">
        <div class="row">
            @include('partials.sidebar');

            <!-- Ubah Kata Sandi -->
            <div class="col-sm-9 admin-ubah-kata-sandi">
                <h1>&nbsp;Ubah Kata Sandi</h1><br>
                @if(session('sukses'))
                    <div class="alert alert-warning" role="alert">
                        {{session('sukses')}}
                    </div>
                @endif
                @if(session('error'))
                    <div class="alert alert-warning" role="alert">
                        {{session('error')}}
                    </div>
                @endif
                <div class="form-ubah-kata-sandi">
                    <form action="/ubah_password" method="POST" role="form">
                        {{csrf_field()}}

                        <div class="form-group {{$errors->has('current-password')?'has-error':''}}">
                            <p>Kata Sandi Lama</p>
                            <input type="password" name="current-password" id="current-password" required>
                            @if($errors->has('current-password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('current-password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('new-password')?'has-error':''}}">
                            <p>Kata Sandi Baru</p>
                            <input type="password" name="new-password" id="new-password" required>
                            @if($errors->has('new-password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('new-password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <p>Konfirmasi Kata Sandi Baru</p>
                            <input type="password" name="new-password-confirm" id="new-password-confirm" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="tombol-hitam">UBAH</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@include('partials.footer');


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>