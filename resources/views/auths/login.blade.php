<!doctype html>
<html lang="en">

@include('partials.header');
@include('partials.navbar');

<body>

    <!-- KONTENT -->
    <div class="form-loginup">
        <span class="text-center">LOGIN SISTEM</span>
        <div class="row">
            <div class="col-6">
                <form id="form_id" method="POST" name="myform" action="{{route('login')}}">
                    {{csrf_field()}}
                    <p class="fontstyle">Nama Pengguna</p>
                    <input type="text" name="username" id="username">
                    <p>Kata Sandi</p>
                    <input type="password" name="password" id="username">
                    <button type="submit" name ="comment" class="button-login">Masuk</button>
                </form>
            </div>
        </div>
    </div>

    <img class="bg-login" src="{{URL::asset('img/bg-login.jpg')}}" alt="bg-login">

@include('partials.footer');

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="assets/js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <!-- SWEET ALERT -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="sweetalert2.all.min.js"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
</body>

</html>
