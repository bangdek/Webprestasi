<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table ='jurusan';
    protected $fillable =['nama', 'ketua jurusan', 'nip'];

    public function siswa()
    {
        return $this->hasMany(Siswa::class);
    }

    public function prestasi()
    {
        return $this->hasMany(Prestasi::class);
    }
}
