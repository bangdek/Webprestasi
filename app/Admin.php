<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admin';

    protected $fillable = ['nama', 'profil'];


    public function getProfile()
    {
        if(!$this->profil) {
            return asset('images/profile.png');
        }

        return asset('images/'.$this->profil);
    }
    
}
