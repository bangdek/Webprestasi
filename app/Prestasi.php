<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestasi extends Model
{
    protected $table = 'prestasi';

    protected $fillable =['nama_kegiatan', 'peringkat', 'tingkat',
    'penyelenggara', 'jml_peserta', 'tgl_mulai', 'tgl_akhir',
    'url_panitia', 'foto_reward', 'foto_penyerahan', 'siswa_id', 'jurusan_id'];

    public function siswa()
    {
        return $this->belongsTo(Siswa::class);
    }

    public function jurusan()
    {
        return $this->belongsTo(Jurusan::class);
    }
}
