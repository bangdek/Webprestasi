<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{

    protected $table = 'siswa';
    
    protected $fillable = ['nama','nis','email','alamat','profil','user_id','jurusan_id'];

    public function getProfile()
    {
        if(!$this->profil) {
            return asset('images/profile.png');
        }

        return asset('images/'.$this->profil);
    }
    
    public function jurusan()
    {
        return $this->belongsTo(Jurusan::class);
    }

    public function prestasi()
    {
        return $this->hasMany(Prestasi::class);
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
