<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Siswa;
use App\Prestasi;
use App\Jurusan;
use Str;

class SiswaController extends Controller
{

    public function ubah_biodata()
    {
        $siswa = auth()->user()->siswa;
        return view('siswa.ubah_biodata',compact(['siswa']));
    }

    public function ubah_biodata_post(Request $request)
    {
        $id = auth()->user()->siswa->id;
        $siswa = Siswa::find($id);
        $siswa->update($request->all());
        return redirect('ubah_biodata')->with('sukses', 'Data berhasil diubah');
    }

    public function ubah_foto()
    {
        $siswa = auth()->user()->siswa;
        return view('siswa.ubah_foto',compact(['siswa']));
    }

    public function ubah_foto_post(Request $request)
    {
        $id = auth()->user()->siswa->id;
        $siswa = Siswa::find($id);
        $siswa->update($request->all());
        if( $request->hasFile('foto-profil')) {
            $request->file('foto-profil')->move('images/', $request->file('foto-profil')->getClientOriginalName());
            $siswa->profil = $request->file('foto-profil')->getClientOriginalName();
            $siswa->save();
        }
        return redirect('ubah_foto')->with('sukses', 'Data berhasil diubah');
    }
}
