<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prestasi;
use App\Siswa;
use App\User;
use Auth;
use Str;

class PrestasiController extends Controller
{
    public function riwayat_prestasi(Request $request)
    {
        $id_siswa = auth()->user()->siswa->id;
        $siswa = Siswa::find($id_siswa);

        if($request->has('cari')){
            $daftar_prestasi = Prestasi::where('nama_kegiatan', 'LIKE', '%' .$request->cari. '%')->get();
        }else {
            $daftar_prestasi = $siswa->prestasi;
        }
        return view('prestasi.riwayat_prestasi', ['daftar_prestasi'=>$daftar_prestasi]);
    }

    public function daftar_prestasi(Request $request)
    {
        if($request->has('cari')){
            $daftar_prestasi = Prestasi::where('nama_kegiatan', 'LIKE', '%' .$request->cari. '%')->get();
        }else {
            $daftar_prestasi = Prestasi::all();
        }
        return view('prestasi.daftar_prestasi', ['daftar_prestasi'=>$daftar_prestasi]);
    }

    public function tambah_prestasi()
    {
        return view('prestasi.tambah_prestasi');
    }

    public function tambah_prestasi_post(Request $request)
    {
        $prestasi = Prestasi::create([
            'siswa_id' => Auth::user()->siswa->id,
            'jurusan_id' => Auth::user()->siswa->jurusan->id,
            'nama_kegiatan' => $request->nama_kegiatan,
            'peringkat' =>$request->peringkat,
            'tingkat' =>$request->tingkat,
            'penyelenggara' =>$request->penyelenggara,
            'jml_peserta' => $request->jml_peserta,
            'tgl_mulai' => $request->tgl_mulai,
            'tgl_akhir' => $request->tgl_akhir,
            'url_panitia' =>$request->url_panitia,
            'foto_reward' =>$request->foto_reward,
            'foto_penyerahan' =>$request->foto_penyerahan
        ]);
        return redirect('tambah_prestasi')->with('sukses', 'Prestasi berhasil ditambahkan');
    }



}
