<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Admin;
use App\User;
use Str;

class AdminController extends Controller
{
    public function daftar_siswa(Request $request)
    {
        if($request->has('cari')){
            $daftar_siswa = Siswa::where('nama', 'LIKE', '%' .$request->cari. '%')->get();
        }
        else {
            $daftar_siswa = Siswa::all();
        }
        return view('siswa.daftar_siswa', ['daftar_siswa'=>$daftar_siswa]);
    }

    public function tambah_siswa()
    {
        return view('siswa.tambah_siswa');
    }

    public function tambah_siswa_post(Request $request)
    {
        //insert ke tabel user
        $user = new User;
        $user->role = 'siswa';
        $user->username = $request->username;
        $user->password = bcrypt('siswa');
        $user->remember_token = Str::random(60);
        $user->save();

        //insert ke tabel siswa
        $request->request->add(['user_id'=>$user->id]);
        $siswa = Siswa::create($request->all());
        return redirect ('daftar_siswa')->with('sukses','Data berhasil ditambahkan');
    }

    public function edit_siswa($id)
    {
        $siswa = Siswa::find($id);
        return view('siswa.edit_siswa', ['siswa'=>$siswa]);
    }

    public function edit_siswa_post(Request $request, $id)
    {
        $siswa = Siswa::find($id);
        $siswa->update($request->all());
        return redirect ('daftar_siswa')->with('sukses', 'Biodata berhasil diubah');
    }

    public function hapus_siswa($id)
    {
        $siswa = Siswa::find($id);
        $siswa->delete($siswa);
        return redirect ('daftar_siswa')->with('sukses', 'Data berhasil dihapus');
    }

    public function ubah_foto()
    {
        $admin = auth()->user()->admin;
        return view('admin.ubah_foto',compact(['admin']));
    }

    public function ubah_foto_post(Request $request)
    {
        $id = auth()->user()->admin->id;
        $admin = Admin::find($id);
        $admin->update($request->all());
        if( $request->hasFile('foto-profil')) {
            $request->file('foto-profil')->move('images/', $request->file('foto-profil')->getClientOriginalName());
            $admin->profil = $request->file('foto-profil')->getClientOriginalName();
            $admin->save();
        }
        return redirect('ubah_foto')->with('sukses', 'Data berhasil diubah');
    }
}
