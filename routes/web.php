<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get  ('/login',      'AuthController@login')->name('login');
Route::get  ('/logout',     'AuthController@logout');
Route::post ('/login',  'AuthController@postlogin');

Route::group(['middleware'=> ['auth','role:admin']], function(){
    Route::get  ('admin/ubah_foto',     'AdminController@ubah_foto');
    Route::get  ('/daftar_prestasi',    'PrestasiController@daftar_prestasi');
    Route::get  ('/daftar_siswa',       'AdminController@daftar_siswa');
    Route::get  ('/siswa/{id}/hapus',   'AdminController@hapus_siswa');
    Route::get  ('/siswa/{id}/ubah',    'AdminController@edit_siswa');
    Route::get  ('/tambah_siswa',       'AdminController@tambah_siswa');
    Route::post ('/admin/ubah_foto',    'AdminController@ubah_foto_post');
    Route::post ('/siswa/{id}/ubah',    'AdminController@edit_siswa_post');
    Route::post ('/tambah_siswa',       'AdminController@tambah_siswa_post');
});

Route::group(['middleware'=>['auth','role:siswa']], function(){
    Route::get  ('/ubah_biodata',       'SiswaController@ubah_biodata');                //beres
    Route::get  ('/ubah_foto',          'SiswaController@ubah_foto');                   //beres
    Route::get  ('/ubah_password',      'AuthController@ubah_password');                //beres
    Route::get  ('/tambah_prestasi',    'PrestasiController@tambah_prestasi');
    Route::get  ('/prestasi',           'PrestasiController@riwayat_prestasi');         //beres
    Route::post ('/ubah_biodata',       'SiswaController@ubah_biodata_post');           //beres
    Route::post ('/ubah_foto',          'SiswaController@ubah_foto_post');              //beres
    Route::post ('/ubah_password',      'AuthController@ubah_password_post');           //beres
    Route::post ('/tambah_prestasi',    'PrestasiController@tambah_prestasi_post');
});

Route::group(['middleware'=>['auth','role:admin,siswa']], function(){
    Route::get  ('/dashboard',      'DashboardController@dashboard');
    Route::get  ('/ubah_password',  'AuthController@ubah_password');
    Route::post ('/ubah_password',  'AuthController@ubah_password_post');
;
});

// Route::get('/', 'DashboardController@dashboard');